package me.uptop.bashim.utils;

public class ConstantManager {
    public static final String AUTH_TOKEN = "AUTH_TOKEN";
    public static final String PATTERN_EMAIL = "^[a-zA-Z_0-9]{3,}@[a-zA-Z_0-9.]{2,}\\.[a-zA-Z0-9]{2,}$";
    public static final String PATTERN_PASSWORD = "^\\S{8,}$";
    public static final String BASKET_COUNT = "BUSKET_COUNT";

    public static final String BASE_URL = "http://anyUrl.ru";
    public static final long MAX_CONNECTION_TIMEOUT = 5000;
    public static final long MAX_READ_TIMEOUT = 5000;
    public static final long MAX_WRITE_TIMEOUT = 5000;

}
