package me.uptop.bashim.ui.adapters;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import me.uptop.bashim.R;

public class QuotesAdapter extends RecyclerView.Adapter<QuotesAdapter.ViewHolder> {
//    List<Quotes> mData;
//
//    public QuotesAdapter(List<Quotes> mData) {
//        this.mData = mData;
//    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView text;
        public ImageButton imageButton;
        public CardView cardView;
        public ViewHolder(View v) {
            super(v);
            cardView = (CardView) v.findViewById(R.id.card_view);
            text = (TextView) v.findViewById(R.id.item_textview_description);
            imageButton = (ImageButton) v.findViewById(R.id.allrecordings_item_imagebutton_favorites);
//            imageButton.setOnClickListener(this);
        }

//        @Override
//        public void onClick(View v) {
//            long position = (long) v.getTag();
//            Toast.makeText(v.getContext(),Long.toString(position),Toast.LENGTH_SHORT).show();
//            if(!liked) {
//                imageButton.setImageResource(R.drawable.ic_star);
//                liked = true;
//            } else {
//                imageButton.setImageResource(R.drawable.ic_star_outline_grey600_24dp);
//                liked = false;
//            }
//
//            favoritesAdd(position);
//        }
//        public void favoritesAdd(long position) {
//            recordings.addToFavorites(position);
//        }

    }


    @Override
    public QuotesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(QuotesAdapter.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }
}