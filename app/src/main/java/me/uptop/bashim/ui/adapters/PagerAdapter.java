package me.uptop.bashim.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import me.uptop.bashim.ui.fragments.AllQuotesFragment;
import me.uptop.bashim.ui.fragments.LikeQuotesFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {
    private static final int mNumOfTabs = 2;

    // tab titles
    private String[] tabTitles = new String[]{"Все записи", "Избранное"};

    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                AllQuotesFragment allRecordingsFragment = new AllQuotesFragment();
                return allRecordingsFragment;
            case 1:
                LikeQuotesFragment likedFragment = new LikeQuotesFragment();
                return likedFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

//    @Override
//    public int getItemPosition(Object object) {
//        return POSITION_NONE;
//    }


//    private List<ProductDto> mProductList = new ArrayList<>();

//    @Override
//    public Fragment getItem(int position) {
//        return ProductFragment.newInstance(mProductList.get(position));
//    }

//    @Override
//    public int getCount() {
//        return mProductList.size();
//    }

//    public void addItem(ProductDto product){
//        mProductList.add(product);
//        notifyDataSetChanged();
//    }

}
