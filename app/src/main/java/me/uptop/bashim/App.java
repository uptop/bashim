package me.uptop.bashim;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import me.uptop.bashim.di.components.AppComponent;
import me.uptop.bashim.di.components.DaggerAppComponent;
import me.uptop.bashim.di.modules.AppModule;

public class App extends Application {
    public static SharedPreferences sSharedPreferences;
    private static Context sContext;
    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
        createDaggerComponent();
        sSharedPreferences= PreferenceManager.getDefaultSharedPreferences(this);
    }

    public static SharedPreferences getSharedPreferences() {
        return sSharedPreferences;
    }

    public static Context getContext() {return sContext;}

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    private void createDaggerComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(sContext))
                .build();
    }
}
