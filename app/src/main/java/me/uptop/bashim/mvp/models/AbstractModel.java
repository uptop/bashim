package me.uptop.bashim.mvp.models;

import javax.inject.Inject;

import me.uptop.bashim.data.managers.DataManager;
import me.uptop.bashim.di.DaggerService;
import me.uptop.bashim.di.components.DaggerModelComponent;
import me.uptop.bashim.di.components.ModelComponent;
import me.uptop.bashim.di.modules.ModelModule;

public abstract class AbstractModel {
    @Inject
    DataManager mDataManager;

    public AbstractModel() {
        ModelComponent component = DaggerService.getComponent(ModelComponent.class);
        if(component == null) {
            component = createDaggerComponent();
            DaggerService.registerComponent(ModelComponent.class, component);
            //DataManagerComponent
        }
        component.inject(this);
    }

    private ModelComponent createDaggerComponent() {
        return DaggerModelComponent.builder()
                .modelModule(new ModelModule())
                .build();
    }
}
