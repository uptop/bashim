package me.uptop.bashim.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.uptop.bashim.data.managers.DataManager;

@Module
public class ModelModule {
    @Provides
    @Singleton
    DataManager provideDataManager() {
        return new DataManager();
    }
}
