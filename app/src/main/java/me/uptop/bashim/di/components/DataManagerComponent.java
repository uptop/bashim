package me.uptop.bashim.di.components;

import javax.inject.Singleton;

import dagger.Component;
import me.uptop.bashim.data.managers.DataManager;
import me.uptop.bashim.di.modules.LocalModule;
import me.uptop.bashim.di.modules.NetworkModule;

@Component(dependencies = AppComponent.class ,modules = {LocalModule.class, NetworkModule.class})
@Singleton
public interface DataManagerComponent {
    void inject(DataManager dataManager);
}
