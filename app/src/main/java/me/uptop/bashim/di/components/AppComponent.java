package me.uptop.bashim.di.components;

import android.content.Context;

import dagger.Component;
import me.uptop.bashim.di.modules.AppModule;

@Component(modules = AppModule.class)
public interface AppComponent {
    Context getContext();
}
