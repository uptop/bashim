package me.uptop.bashim.di.components;

import javax.inject.Singleton;

import dagger.Component;
import me.uptop.bashim.di.modules.ModelModule;
import me.uptop.bashim.mvp.models.AbstractModel;

@Component(modules = ModelModule.class)
@Singleton
public interface ModelComponent {
    void inject(AbstractModel abstractModel);
}
