package me.uptop.bashim.data.managers;

import android.content.Context;

import javax.inject.Inject;

import me.uptop.bashim.App;
import me.uptop.bashim.data.network.RestService;
import me.uptop.bashim.di.DaggerService;
import me.uptop.bashim.di.components.DaggerDataManagerComponent;
import me.uptop.bashim.di.components.DataManagerComponent;
import me.uptop.bashim.di.modules.LocalModule;
import me.uptop.bashim.di.modules.NetworkModule;

public class DataManager {
    private Context mContext;

    @Inject
    RestService mRestService;
    @Inject
    PreferencesManager mPreferencesManager;

    public DataManager() {
        this.mContext = App.getContext();

        DataManagerComponent component = DaggerService.getComponent(DataManagerComponent.class);
        if(component == null) {
            component = DaggerDataManagerComponent.builder()
                    .appComponent(App.getAppComponent())
                    .localModule(new LocalModule())
                    .networkModule(new NetworkModule())
                    .build();
            DaggerService.registerComponent(DataManagerComponent.class, component);
        }
        component.inject(this);

//        generateMockData();
    }

    public Context getContext(){return mContext;}

}
